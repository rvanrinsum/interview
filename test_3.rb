require "minitest/autorun"
require_relative "person"

class BrokenedTest < Minitest::Test
  def test_name
    person = Person.new "Yukihiro", "Matsumoto"
    assert_equal("Yukihiro Matsumoto", person.name)
  end
end
