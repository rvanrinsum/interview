require "minitest/autorun"

class Person
  def initialize first_name, last_name
    @first_name = first_name
    @last_name = last_name
  end

  def name
  end
end

class PersonTest < Minitest::Test
  def test_name
    person = Person.new "Yukihiro", "Matsumoto"
    assert_equal("Yukihiro Matsumoto", person.name)
  end
end
