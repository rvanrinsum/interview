require "minitest/autorun"

class Brokened
  def uh_oh
    "It needs fixing!"
  end
end

class BrokenedTest < Minitest::Test
  def test_uh_oh
    actual = Brokened.new
    assert_equal("I'm all better!", actual.uh_oh)
  end
end
