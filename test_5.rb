require "minitest/autorun"

class Person
  def initialize first_name, last_name
    @first_name = first_name
    @last_name = last_name
  end
end

class LanguageTest < Minitest::Test
  def test_nicknames
    person = Person.new "Yukihiro", "Matsumoto"
    person.add_language "Ruby"
    person.add_language "Javascript"
    person.add_language "Elixir"
    assert_equal(person.languages,
                 ["Ruby", "Javascript", "Elixir"])
  end

  def test_ordered_nicknames
    person = Person.new "Yukihiro", "Matsumoto"
    person.add_language "Ruby"
    person.add_language "Javascript"
    person.add_language "Elixir"
    assert_equal(person.ordered_languages,
                 ["Elixir", "Javascript", "Ruby"])
  end

  def test_longest_nickname
    person = Person.new "Yukihiro", "Matsumoto"
    person.add_language "Ruby"
    person.add_language "Javascript"
    person.add_language "Elixir"
    assert_equal(person.longest_language, "Javascript")
  end
end
