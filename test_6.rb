require "minitest/autorun"

class Person
  def initialize first_name, last_name
    @first_name = first_name
    @last_name = last_name
  end
end

class AlterEgo
end

class CarTest < Minitest::Test
  def test_alter_ego
    person = Person.new "Yukihiro", "Matsumoto"
    alter_ego = AlterEgo.new name: "Matz", age: 33
    person.alter_ego = alter_ego
    assert_equal(person.alter_ego.age, 33)
  end
end
