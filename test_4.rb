require "minitest/autorun"

class Car

  def initialize brand: 'Volvo', model: 'V70'
    @brand = brand
    @model = model
  end

  attr_accessor :miles_driven

  def add_mile
  end

end

class CarTest < Minitest::Test
  def test_add_mile
    car = Car.new
    car.add_mile
    assert_equal(1, car.miles_driven)
    car.add_mile
    assert_equal(2, car.miles_driven)
  end
end
